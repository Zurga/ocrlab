import sys
import os
import json

import numpy as np
import pandas as pd
import cv2
import seaborn as sns

from sklearn.neighbors import LocalOutlierFactor



# loading images
def read_images(files):
    return [cv2.imread(f) for f in files]


def preprocess(image):
    # Make it grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # clean the image using otsu method with the inversed binarized image
    ret, th = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    return th


#processing letter by letter boxing
def process_letter(thresh):
    # assign the kernel size
    kernel = np.ones((2, 1), np.uint8) # vertical
    # use closing morph operation then erode to narrow the image
    temp_img = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel, iterations=3)
    # temp_img = cv2.erode(thresh,kernel,iterations=2)
    letter_img = cv2.erode(temp_img, kernel, iterations=1)
    # find contours
    (contours, _) = cv2.findContours(letter_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    return list(boxes(contours))


#processing letter by letter boxing
def process_word(thresh):
    # assign 2 rectangle kernel size 1 vertical and the other will be horizontal
    kernel = np.ones((2, 1), np.uint8)
    kernel2 = np.ones((1, 4), np.uint8)
    # use closing morph operation but fewer iterations than the letter then erode to narrow the image
    temp_img = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel, iterations=2)
    #temp_img = cv2.erode(thresh,kernel,iterations=2)
    word_img = cv2.dilate(temp_img, kernel2, iterations=1)

    (contours, _) = cv2.findContours(word_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    return list(boxes(contours))


#processing line by line boxing
def process_line(thresh):
    # assign a rectangle kernel size	1 vertical and the other will be horizontal
    kernel = np.ones((1, 5), np.uint8)
    kernel2 = np.ones((2, 4), np.uint8)
    # use closing morph operation but fewer iterations than the letter then erode to narrow the image
    temp_img = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel2, iterations=2)
    #temp_img = cv2.erode(thresh,kernel,iterations=2)
    line_img = cv2.dilate(temp_img, kernel, iterations=5)

    (contours, _) = cv2.findContours(line_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    return list(boxes(contours))


#processing par by par boxing
def process_par(thresh):
    # assign a rectangle kernel size
    kernel = np.ones((5, 5), 'uint8')
    par_img = cv2.dilate(thresh, kernel, iterations=3)

    (contours, _) = cv2.findContours(par_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    return list(boxes(contours))


#processing margin with paragraph boxing
def process_margin(thresh):
    # assign a rectangle kernel size
    kernel = np.ones((20, 5), 'uint8')
    margin_img = cv2.dilate(thresh, kernel, iterations=5)

    (contours, _) = cv2.findContours(margin_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    return list(boxes(contours))


def draw_boxes(boxes, image, colors=None):
    if colors is None:
        colors = ((0, 0, 0),) * len(boxes)
    elif len(colors) == 3:
        colors = (colors,) * len(boxes)
	# loop in all the contour areas
    for (x, y, w, h), color in zip(boxes, colors):
        cv2.rectangle(image, (x - 1, y - 5), (x + w, y + h), color, 1)

    return image


def boxes(contours):
    for cnt in contours:
        yield cv2.boundingRect(cnt)


def build_data(boxes):
    return [(x, y, w, h, (x + w) / 2, (y + h) / 2)
            for x, y, w, h in boxes]


def grow_area(boxes):
    left, top, width, height = 9999, 9999, 0, 0

    for b_left, b_top, b_w, b_h in boxes:
        new_left = min(b_left, left)
        new_top = min(b_top, top)
        new_width = max(b_left + b_w, width)
        new_height = max(b_top + b_h, height)
        left = new_left
        top = new_top
        width = new_width
        height = new_height

    return left - 1, top - 5, (width - left) + 4, (height - top) + 2


def detect_outliers(data, clf):
    return clf.fit_predict(data)


def main(image_files):
    images = read_images(image_files)
    clf = LocalOutlierFactor()
    results = []

    for fle, image in zip(image_files, images):
        threshold = preprocess(image)
        values = process_par(threshold)
        data = build_data(values)
        # Detect outliers
        liers = detect_outliers(data, clf)
        colors = [(0, 255, 0) if lier == 1 else (255, 0, 0) for lier in liers]
        boxes = [(x, y, w, h)  for ((x, y, w, h, c_x, c_y), lier) in zip(data, liers)
                 if lier == 1 or lier == 0]
        results.append(grow_area(boxes))

        cv2.waitKey(0)
    print(json.dumps(results))


if __name__ == "__main__":
    main(sys.argv[1:])
