#!/bin/bash

folder=$1
filename=$2
resolution=$3
gui_resolution=100
pushd `pwd`/$folder
# pdftoppm -r 300 -gray "$2" output
# convert -density 300 -depth 8 -quality 85 "$folder/$2" a.png
# convert -trim -density "$resolution" -border 30x30 -bordercolor white $filename -quality 100 ocr.jpg
gs -dNumberRenderingThreads=4 -dNOGC -SDEVICE=jpeg -r300x300 -sOutputFile="ocr_%04d.jpg" -dNOPAUSE -dBATCH -- $filename
gs -dNumberRenderingThreads=4 -dNOGC -SDEVICE=jpeg -r100x100 -sOutputFile="gui_%04d.jpg" -dNOPAUSE -dBATCH -- $filename
popd
