defmodule Ocrlab.Repo.Migrations.CreatePages do
  use Ecto.Migration

  def change do
    create table(:pages) do
      add :pdf_id, references(:pdfs, ondelete: :delete_all)

      timestamps()
    end

  end
end
