defmodule Ocrlab.Repo.Migrations.AddedHasMany do
  use Ecto.Migration

  def change do
    alter table("images") do
      add :height, :integer
      add :width, :integer
      add :resolution, :integer
    end

    alter table("documents") do
      add :file, :string
      add :uuid, :string
    end

    alter table("pages") do
      remove :pdf_id
      add :document_id, references("documents", [ondelete: :delete_all])
    end
  end
end
