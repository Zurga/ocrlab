defmodule Ocrlab.Repo.Migrations.AddUuidToPageAndAreas do
  use Ecto.Migration

  def change do
    alter table("areas") do
      add :uuid, :string
    end

  end
end
