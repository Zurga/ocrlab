defmodule Elixir.Ocrlab.Repo.Migrations.Added_text_to_pages do
  use Ecto.Migration

  def change do
    alter table("pages") do
      add :text, :string
    end
  end
end
