defmodule Ocrlab.Repo.Migrations.AddUuidToPageAndImages do
  use Ecto.Migration

  def change do
    alter table("pages") do
      add :uuid, :string
    end

    alter table("images") do
      add :uuid, :string
    end
  end
end
