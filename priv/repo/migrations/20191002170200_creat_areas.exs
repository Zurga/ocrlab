defmodule Ocrlab.Repo.Migrations.CreateAreas do
  use Ecto.Migration

  def change do
    create table(:areas) do
      add :x, :integer
      add :y, :integer
      add :w, :integer
      add :h, :integer
      add :type, :string
      add :order, :integer

      add :page_id, references(:pages, ondelete: :delete_all)

      timestamps()
    end

  end
end
