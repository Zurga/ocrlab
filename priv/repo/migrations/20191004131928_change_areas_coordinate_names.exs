defmodule Ocrlab.Repo.Migrations.ChangeAreasCoordinateNames do
  use Ecto.Migration

  def change do
    rename table("areas"), :w, to: :width
    rename table("areas"), :h, to: :height
    rename table("areas"), :x, to: :left
    rename table("areas"), :y, to: :top
  end
end
