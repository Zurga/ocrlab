defmodule Elixir.Ocrlab.Repo.Migrations.Add_text_to_area do
  use Ecto.Migration

  def change do
    alter table("areas") do 
      add :text, :string

    end
  end
end
