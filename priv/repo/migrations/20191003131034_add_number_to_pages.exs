defmodule Ocrlab.Repo.Migrations.AddNumberToPages do
  use Ecto.Migration

  def change do
    alter table("pages") do
      add :number, :integer
    end

  end
end
