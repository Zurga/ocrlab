defmodule Ocrlab.Repo.Migrations.AddPageToImage do
  use Ecto.Migration

  def change do
    alter table("images") do
      add :page_id, references("pages", ondelete: :delete_all)
    end

  end
end
