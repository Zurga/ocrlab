defmodule Elixir.Ocrlab.Repo.Migrations.Addedpdf do
  use Ecto.Migration

  def change do
    create table(:pdfs) do
      add :file, :string
      add :uuid, :string
      add :document_id, references(:documents, ondelete: :delete_all)

      timestamps()
    end

  end
end
