defmodule Ocrlab.Repo.Migrations.MoveTimestampsToPostgres do
  use Ecto.Migration

  def change do
    alter table("pages") do 
      modify :inserted_at, :utc_datetime, default: fragment("NOW()") 
      modify :updated_at, :utc_datetime, default: fragment("NOW()") 
    end

    alter table("images") do 
      modify :inserted_at, :utc_datetime, default: fragment("NOW()") 
      modify :updated_at, :utc_datetime, default: fragment("NOW()") 
    end

    alter table("areas") do 
      modify :inserted_at, :utc_datetime, default: fragment("NOW()") 
      modify :updated_at, :utc_datetime, default: fragment("NOW()") 
    end

    alter table("documents") do 
      modify :inserted_at, :utc_datetime, default: fragment("NOW()") 
      modify :updated_at, :utc_datetime, default: fragment("NOW()") 
    end

    alter table("pages") do 
      modify :inserted_at, :utc_datetime, default: fragment("NOW()") 
      modify :updated_at, :utc_datetime, default: fragment("NOW()") 
    end
  end
end
