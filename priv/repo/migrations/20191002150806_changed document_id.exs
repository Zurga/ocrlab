defmodule Elixir.Ocrlab.Repo.Migrations.ChangeddocumentId do
  use Ecto.Migration

  def change do
    alter table(:pdfs) do
      remove :document
      add :document_id, references(:documents, ondelete: :delete_all)
    end
  end
end
