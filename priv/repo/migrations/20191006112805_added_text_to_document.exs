defmodule Ocrlab.Repo.Migrations.AddedTextToDocument do
  use Ecto.Migration

  def change do
    alter table("documents") do
      add :text, :string
    end

  end
end
