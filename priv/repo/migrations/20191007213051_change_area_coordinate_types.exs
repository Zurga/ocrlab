defmodule Ocrlab.Repo.Migrations.ChangeAreaCoordinateTypes do
  use Ecto.Migration

  def change do
    alter table("areas") do
      modify :top, :float
      modify :width, :float
      modify :left, :float
      modify :height, :float
    end

  end
end
