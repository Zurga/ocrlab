defmodule Ocrlab.Repo.Migrations.AddTypeToImage do
  use Ecto.Migration

  def change do
    alter table("images") do
      add :type, :string
    end

  end
end
