// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import css from "../css/app.css"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import "phoenix_html"
import LiveSocket from "phoenix_live_view";     


import $ from "jquery";
import {Socket} from "phoenix";
import {fabric} from "fabric";
import Tesseract from "tesseract.js";
import mode_markdown from "ace-builds";
import theme_twilight from "ace-builds";

console.log(mode_markdown);

var DrawArea = (function () {
    var origX, origY
    function DrawArea(canvas) {
        var inst=this;
        this.canvas = canvas;
        this.className= 'DrawArea';
        this.isDrawing = 0;
        this.bindEvents();
    }

	DrawArea.prototype.bindEvents = function() {
    var inst = this;
    inst.canvas.on('mouse:down', function(o) {
      inst.onMouseDown(o);
    });
    inst.canvas.on('mouse:move', function(o) {
      inst.onMouseMove(o);
    });
    inst.canvas.on('mouse:up', function(o) {
      inst.onMouseUp(o);
    });
    inst.canvas.on('object:moving', function(o) {
      inst.disable();
    })
    }

    DrawArea.prototype.onMouseUp = function (o) {
      var inst = this

      switch (inst.isDrawing) {
          case 0:
            break;
          // Drawing an area
          case 1:
            inst.disable();
            var activeObj = inst.canvas.getActiveObject();

            activeObj.set({hasControls: true});

            if (Math.abs(activeObj.width) < 10 & Math.abs(activeObj.height) < 10) {
                inst.canvas.remove(activeObj);
            } else {
                console.log('creating area', o);

                push_area(liveSocket, inst.canvas, "area-created", o.target);
            }
            break;
          // Drawing an arrow
          case 2:
            console.log(o);
            inst.disable();
            var activeObj = inst.canvas.getActiveObject();
      }
    };

    DrawArea.prototype.onMouseMove = function (o) {
        var inst = this;
        if(!inst.isEnable()){ return; }

        var pointer = inst.canvas.getPointer(o.e);
        var activeObj = inst.canvas.getActiveObject();

        //TODO set bounding of the area to the width and height
        //of the canvas. Maybe use a switch or something.
        if (pointer.x >= canvas.width) {
            activeOjb.set({left: canvas.width});
        }
        
        //TODO fix creation of areas from right to left.
        if(origX > pointer.x){
            activeObj.set({ left: Math.abs(pointer.x) }); 
        }
        if(origY > pointer.y){
            activeObj.set({ top: Math.abs(pointer.y) });
        }

        activeObj.set({ width: Math.abs(origX - pointer.x) });
        activeObj.set({ height: Math.abs(origY - pointer.y) });

        activeObj.setCoords();
        inst.canvas.renderAll();
    };

    DrawArea.prototype.onMouseDown = function (o) {
        var inst = this;
        console.log(o);
      if (o.target == null) {
        inst.draw_area(inst, o);
      } else {
            if (o.e.ctrlKey) {
                inst.draw_arrow(inst, o);
            }
      }
    };

    DrawArea.prototype.draw_area = function (inst, o) {
        inst.enable();

        var pointer = inst.canvas.getPointer(o.e);
        origX = pointer.x;
        origY = pointer.y;

        var area_type = function(button) {
          if (button === 2) {
              return "image";
          } else {
              return 'text';
          }
        }(o.e.button);

        var area = create_area(
          origX, 
          origY, 
          pointer.x - origX, 
          pointer.y - origY,
          area_type)

        inst.canvas.add(area).setActiveObject(area);
    }
    
    DrawArea.prototype.draw_arrow = function (o) {
    }

    DrawArea.prototype.isEnable = function(){
      return this.isDrawing;
    }

    DrawArea.prototype.enable = function(){
      this.isDrawing = 1;
    }

    DrawArea.prototype.disable = function(){
      this.isDrawing = 0;
    }

    return DrawArea;
}());


function drawServerAreas(canvas, areas) {
    for (var area of areas) {
        console.log(area);
        if (area.type == "image" || area.type == "text" || area.type == "line") {
            var area = create_area(area.left, area.top, area.width, area.height,
            area.type, area.uuid)
            canvas.add(area);
        }
    }
}

function mount_canvas(liveview) {
    var canvas_el = liveview.el;
    var canvas = new fabric.Canvas(canvas_el.id, 
        {
            altSelectionKey: 'shiftKey',
            fireRightClick: true,
            stopContextMenu: true,
            selection: false,
            selectionKey: 'shiftKey',
            width: Number(liveview.el.dataset.width),
            height: Number(liveview.el.dataset.height) 
        }
    );

    window.canvas[canvas_el.id] = canvas;

    canvas.setBackgroundImage(liveview.el.dataset.background, 
        canvas.renderAll.bind(canvas), {
        originX: 'left',
        originY: 'top'
    });

    var drawArea = new DrawArea(canvas);

    for (event of update_events) {
        canvas.on('object:' + event, function (o) {
            var area = applyUpdatedTransforms(o)
            push_area(liveSocket, this, "area-updated", area)
        });
    }

    bindDeleteArea(canvas);
}

window.editor = null
let update_events = ["modified"];
let Hooks = {};
window.canvas = {}

Hooks.Pages = {
    mounted() {
        mount_canvas(this);
        if (window.editor != null) {
            window.editor = mode_markdown.edit("editor");
        }
        //editor.setTheme(theme_twilight);
    },
    updated() {
        var canvas = window.canvas[this.el.id];
        canvas.initialize(this.el);
    }
}

Hooks.Areas = {
    mounted() {
        var canvas = window.canvas[this.el.dataset.page];
        console.log(canvas);
        var areas = JSON.parse(this.el.content);
        for (var object of canvas.getObjects()) {
            canvas.remove(object)
        }
        drawServerAreas(canvas, areas) 
    },
    updated() {
        var canvas = window.canvas[this.el.dataset.page];
        var areas = JSON.parse(this.el.content);
        for (var object of canvas.getObjects()) {
            canvas.remove(object)
        }
        drawServerAreas(canvas, areas) 
    }
}


// Create the actual socket
let liveSocket = new LiveSocket("/live", Socket, {hooks: Hooks});
liveSocket.connect();
window.liveSocket = liveSocket;


/* The functions that will allow us to create Areas */
function create_area(left, top, width, height, area_type, uuid) {
    return new fabric.Rect({
        left: left,
        top: top,
        width: width,
        height: height,
        area_type: area_type,
        fill: area_color(area_type),
        originX: 'left',
        originY: 'top',
        angle: 0,
        transparentCorners: true,
        hasControls: true,
        perPixelTargetFind: true,
        hasRotatingPoint: false,
        opacity: 0.3,
        uuid: (typeof uuid !== 'undefined') ? uuid : create_UUID(),
    })
}

function delete_area(canvas) {
    var area = canvas.getActiveObject();
    var area_json = area.toJSON(["area_type", "uuid"]);
    canvas.remove(area);
    push_area(liveSocket, canvas, "area-removed", area_json)
}

function area_color(type) {
    switch (type) {
        case "text":
            return "green";
        case "image":
            return "blue"
        default:
            return "orange"
    }
}

function create_UUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}

function push_area(liveSocket, canvas, event_type, area){
    console.log('push_area');
    console.log(event_type);
    console.log('area', area);

    var livesocket_id = document.getElementsByClassName("liveview")[0].id;

    if (area.toJSON != undefined ) {
        area = area.toJSON(["area_type", "uuid"]);
    }
    area.type = area.area_type;

    liveSocket.views[livesocket_id].pushWithReply("event", {
        type: "phx-click",
        event: event_type,
        value: "page_uuid=" + canvas.lowerCanvasEl.id + '&' + 'area=' + JSON.stringify(area)
    })
}

function bindDeleteArea(canvas) {
    canvas.wrapperEl.tabIndex = 1000;
    canvas.wrapperEl.addEventListener("keydown", 
    function (e) { 
        if (e.key == "Backspace") {
            delete_area(canvas) 
        }
    }, false);
}

function applyUpdatedTransforms(e) {
    var area = e.transform.target.toJSON(["area_type", "uuid"]);
    switch (e.transform.action) {
        case "drag":
            break;

        case "scaleY":
            area.height = e.transform.newScaleY * area.height;
            break;
        case "scaleX":
            area.width = e.transform.newScaleX * area.width;
            break;
        default:
            area.width = e.transform.newScaleX * area.width;
            area.height = e.transform.newScaleY * area.height;
    }
    return area
}

window.scrollTo = function () {
    console.log(this);
    console.log("something");
}
