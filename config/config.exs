# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :ocrlab,
  ecto_repos: [Ocrlab.Repo]

# Configures the endpoint
config :ocrlab, OcrlabWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "OAg8wL4cUl0E4t9/0jMQXRClMJ92ocXNRLASlInU4/9VPvRVt39MevP4RsrRYkhv",
  render_errors: [view: OcrlabWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Ocrlab.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "sososalty"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :arc,
  storage: Arc.Storage.Local

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
