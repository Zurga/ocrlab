defmodule Ocrlab.Documents do
  @moduledoc """
  The Documents context.
  """
  require Protocol

  import Ecto.Query, warn: false
  import Ocrlab.ImageUtil
  import Mogrify
  alias Ocrlab.Ocr

  alias Ocrlab.Repo

  alias Ocrlab.Documents.Document
  alias Ocrlab.Documents.Image
  alias Ocrlab.Documents.Area
  alias Ocrlab.Documents.Page
  Protocol.derive(Jason.Encoder, Area, only: [:left, :top, :width, :height, :type, :uuid])

  @doc """
  Returns the list of documents.

  ## Examples

      iex> list_documents()
      [%Document{}, ...]

  """
  def list_documents do
    Repo.all(Document)
  end

  @doc """
  Gets a single document.

  Raises `Ecto.NoResultsError` if the Document does not exist.

  ## Examples

      iex> get_document!(123)
      %Document{}

      iex> get_document!(456)
      ** (Ecto.NoResultsError)

  """
  def get_document!(id) do
    document = Repo.get!(Document, id)
    |> Repo.preload(pages: [:ocr_image, :gui_image, :text_areas, :image_areas])

    %Document{document | 
      pages: Enum.sort(document.pages, &(&1.number < &2.number))
    }
  end

  @doc """
  Creates a document.

  ## Examples

      iex> create_document(%{field: value})
      {:ok, %Document{}}

      iex> create_document(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_document(attrs \\ %{}) do
    {:ok, document} = %Document{}
                     |> Document.changeset(attrs) 
                     |> Repo.insert()

    directory = "uploads/#{document.uuid}/"
    {ocr_images, gui_images} = convert_pdf(directory, document.file.file_name, 300)

    pages = create_pages(document, length ocr_images)
    {_, ocr_images} = create_images(pages, ocr_images, "ocr")
    {_, gui_images} = create_images(pages, gui_images, "gui")

    # Create all the areas we need
    pages = Repo.preload pages, [:gui_image, :ocr_image]

    bulk_areas = initial_areas(pages)
                 |> Enum.zip(pages)
                 |> Enum.reduce([], fn {area, page}, inserts ->
                   inserts ++ [prepare_bulk_area(page, area)]
                 end) 

    {_, areas} = Repo.insert_all(Area, bulk_areas, [returning: true])
    pages = Enum.zip(areas, pages)
      |> Enum.map(fn {area, page} ->
        %Page{page | text_areas: [area]}
      end)
    update_document(document, %{pages: pages})
  end

  @doc """
  Updates a document.

  ## Examples

      iex> update_document(document, %{field: new_value})
      {:ok, %Document{}}

      iex> update_document(document, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_document(%Document{} = document, attrs) do
    document
    |> Document.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Document.

  ## Examples

      iex> delete_document(document)
      {:ok, %Document{}}

      iex> delete_document(document)
      {:error, %Ecto.Changeset{}}

  """
  def delete_document(%Document{} = document) do
    Repo.delete(document)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking document changes.

  ## Examples

      iex> change_document(document)
      %Ecto.Changeset{source: %Document{}}

  """
  def change_document(%Document{} = document) do
    Document.changeset(document, %{})
  end

  @doc """
  Returns the list of images.

  ## Examples

      iex> list_image()
      [%Image{}, ...]

  """
  def list_image do
    Repo.all(Image)
  end

  @doc """
  Gets a single image.

  Raises `Ecto.NoResultsError` if the Image does not exist.

  ## Examples

      iex> get_image!(123)
      %Image{}

      iex> get_image!(456)
      ** (Ecto.NoResultsError)

  """
  def get_image!(id), do: Repo.get!(Image, id)

  @doc """
  Creates an image.

  ## Examples

      iex> create_image(%{field: value})
      {:ok, %Image{}}

      iex> create_image(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_image(attrs \\ %{}) do
    image = open(attrs.file)
    image_data = verbose(image)
    attrs = %{
      type: attrs.type,
      file: attrs.file,
      height: image_data.height,
      width: image_data.width,
      resolution: attrs.resolution}
    %Image{}
    |> Image.changeset(attrs)
    |> Repo.insert()
  end

  def create_images(pages, images, type) do
    images = Stream.zip(pages, images)
      |> Stream.map(fn {page, attrs} ->
        image = open(attrs.file)
        image_data = verbose(image)
        Ecto.Changeset.apply_changes(Image.changeset(%Image{}, 
         %{
          type: type,
          file: attrs.file,
          height: image_data.height,
          width: image_data.width,
          resolution: attrs.resolution,
          page_id: page.id
         }
        ))
      end)
      |> Enum.map(&clean_attrs/1)
    Repo.insert_all(Image, images, [returning: :true])
  end


  @doc """
  Updates an image.

  ## Examples

      iex> update_image(image, %{field: new_value})
      {:ok, %Image{}}

      iex> update_image(image, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_image(%Image{} = image, attrs) do
    image
    |> Image.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes an Image.

  ## Examples

      iex> delete_image(image)
      {:ok, %Image{}}

      iex> delete_image(image)
      {:error, %Ecto.Changeset{}}

  """
  def delete_image(%Image{} = image) do
    Repo.delete(image)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking image changes.

  ## Examples

      iex> change_image(image)
      %Ecto.Changeset{source: %Image{}}

  """
  def change_image(%Image{} = image) do
    Image.changeset(image, %{})
  end

  @doc """
  Returns the list of area.

  ## Examples

      iex> list_area()
      [%Area{}, ...]

  """
  def list_area do
    Repo.all(Area)
  end

  @doc """
  Gets a single area.

  Raises `Ecto.NoResultsError` if the Area does not exist.

  ## Examples

      iex> get_area!(123)
      %Area{}

      iex> get_area!(456)
      ** (Ecto.NoResultsError)

  """
  def get_area!(id), do: Repo.get!(Area, id)

  @doc """
  Creates a area.

  ## Examples

      iex> create_area(%{field: value})
      {:ok, %Area{}}

      iex> create_area(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_area(%Page{} = page, attrs \\ %{}) do
    %Area{}
    |> Area.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:page, page)
    |> Repo.insert()
  end

  def prepare_bulk_area(page, area) do
    # areas = prepare_insert_all(pages, Area, area_attrs, :page, :page_id)
    Area.changeset(%Area{}, Map.put(area, :page_id, page.id))
    |> Ecto.Changeset.apply_changes()
    |> clean_attrs()
  end


  def create_areas(page, areas) do
    areas = Enum.map(areas, &(prepare_bulk_area(page, &1)))
    {_number, areas} = Repo.insert_all(Area, areas, returning: :true)
    areas
  end

  defp clean_attrs(item) do
    item
    |> Map.from_struct()
    |> Enum.reject(fn 
      {_key, nil} ->
        true
      {key, %_struct{}} ->
        true
      _other -> 
        false
    end) 
  end

  defp prepare_insert_all(relations, item_type, items, relation_key, id_key) do
    item_type = struct(item_type)
    Enum.zip(relations, items)
      |> Enum.map(fn {relation, item} ->
        item_type
        |> item_type.changeset(item)
        |> Ecto.Changeset.put_assoc(relation_key, relation)
      end)
      |> Stream.map(&Ecto.Changeset.apply_changes/1)
      |> Enum.map(fn item ->
        item
        |> Map.from_struct(item)
        |> Map.put(id_key, Map.get(item, relation_key).id)
        |> Stream.reject(fn 
          {_key, nil} ->
            true
          {key, %_struct{}} ->
            true
          _other -> 
            false
        end) 
      end)
  end


  @doc """
  Updates a area.

  ## Examples

      iex> update_area(area, %{field: new_value})
      {:ok, %Area{}}

      iex> update_area(area, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_area(%Area{} = area, attrs) do
    area
    |> Area.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes an Area.

  ## Examples

      iex> delete_area(area)
      {:ok, %Area{}}

      iex> delete_area(area)
      {:error, %Ecto.Changeset{}}

  """
  def delete_area(%Area{} = area) do
    Repo.delete(area)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking area changes.

  ## Examples

      iex> change_area(area)
      %Ecto.Changeset{source: %Area{}}

  """
  def change_area(%Area{} = area) do
    Area.changeset(area, %{})
  end

  @doc """
  Returns the list of page.

  ## Examples

      iex> list_page()
      [%Page{}, ...]

  """
  def list_page do
    Repo.all(Page)
  end

  @doc """
  Gets a single page.

  Raises `Ecto.NoResultsError` if the Page does not exist.

  ## Examples

      iex> get_page!(123)
      %Page{}

      iex> get_page!(456)
      ** (Ecto.NoResultsError)

  """
  def get_page!(id) do 
    Repo.get!(Page, id)
    |> Repo.preload([:gui_image, :ocr_image])
  end

  @doc """
  Creates a page.

  ## Examples

      iex> create_page(%{field: value})
      {:ok, %Page{}}

      iex> create_page(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_page(%Document{} = document, attrs \\ %{}) do
    # IO.inspect attrs
    %Page{}
    |> Page.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:document, document)
    |> Repo.insert()
  end

  def create_pages(%Document{} = document, amount) do
    prepared = 1..amount
      |> Stream.map(fn number -> 
       attrs = %{
         number: number,
         document_id: document.id
       }
      end)
      |> Stream.map(&(Page.changeset(%Page{}, &1)))
      |> Stream.map(&Ecto.Changeset.apply_changes/1)
      |> Enum.map(&clean_attrs/1)

    {_num, inserted} = Repo.insert_all(Page, prepared, returning: [:id])
    inserted
  end

  @doc """
  Updates a page.

  ## Examples

      iex> update_page(page, %{field: new_value})
      {:ok, %Page{}}

      iex> update_page(page, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_page(%Page{} = page, attrs) do
    page
    |> Page.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Page.

  ## Examples

      iex> delete_page(page)
      {:ok, %Page{}}

      iex> delete_page(page)
      {:error, %Ecto.Changeset{}}

  """
  def delete_page(%Page{} = page) do
    Repo.delete(page)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking page changes.

  ## Examples

      iex> change_page(page)
      %Ecto.Changeset{source: %Page{}}

  """
  def change_page(%Page{} = page) do
    Page.changeset(page, %{})
  end

  @doc """
  Returns an OCR'ed version of a page. 
  """
  def ocr(%Document{uuid: document_uuid, pages: pages} = document) do
    %Document{document | 
      pages: Enum.reduce(pages, [], fn (page, result) -> 
        [ocr(document, page) | result]
      end) |> Enum.reverse()
    }
  end
  
  def ocr(
    %Document{uuid: document_uuid},
    %Page{
      ocr_image: ocr_image, 
      gui_image: gui_image, 
      uuid: uuid, 
      text_areas: text_areas,
      image_areas: image_areas} = page
  ) do
    if text_areas == [] and image_areas == [] do 
      page
    else
      image_areas_path = "uploads/#{document_uuid}/#{uuid}/"
      ocr_text_path = create_ocr_area(uuid, ocr_image, text_areas, image_areas)
      images = extract_images(image_areas_path, image_areas, ocr_image)

      hocr = Ocr.ocr(ocr_text_path)
      # body = Floki.parse(hocr) |> Floki.find("body > *") |> Floki.raw_html()
    
      %Page{page | text: hocr}
    end
  end
end
