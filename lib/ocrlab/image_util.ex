import Mogrify


defmodule Ocrlab.ImageUtil do
  def convert_pdf(directory, file_name, resolution \\ 300) do
    # Convert the pdf to pictures per page
    IO.puts directory
    System.cmd("/home/jim/git/ocrlab/scripts/pdfconvert.sh", 
      [directory, file_name, "#{resolution}"])

    {:ok, files} = File.ls(directory)  

    IO.puts "Files"
    {sort_files(files, ~r/ocr.*\.jpg/)
    |> Enum.map(fn file ->
      %{
        file: Path.join(directory, file),
        resolution: 300
      }
    end),
    sort_files(files,~r/gui.*\.jpg/ )
    |> Enum.map(fn file ->
      %{
        file: Path.join(directory, file),
        resolution: 100
      }
    end)}
  end

  defp sort_files(files, regex) do
    files
    |> Enum.filter(&(String.match?(&1, regex)))
    |> Enum.sort(fn (one, other) -> 
      number = ~r/(\d+)\.jpg/
      one = String.to_integer(List.first Regex.run(number, one, capture: :all_but_first)) 
      other = String.to_integer(List.first Regex.run(number, other, capture: :all_but_first)) 
      one < other
    end)

  end

  def rectangle(image, left, top, width, height, color) do
    image
    |> custom("fill", color)
    |> custom("draw", "rectangle #{left},#{top} #{left + width},#{top + height}")
  end

  def draw_rectangles(image, areas, color\\"black") do
    Enum.reduce(areas, image, fn (area, image) -> 
      rectangle(image, area.left, area.top, area.width, area.height, color)
    end)
  end

  def stitch_files(files, output_name) do
    %Mogrify.Image{path: "{output_name}.png", ext: "png"}
    |> custom("-append", files)
    |> IO.inspect
    |> create(path: "/tmp/")
    "/tmp/#{output_name}.png}"
  end

  def create_ocr_area(filename, ocr_source, text_areas, non_text_areas) do
    path = "/tmp/#{filename}_mask.png"
    ocrable_path = "/tmp/#{filename}_ocr.png"
    %Mogrify.Image{path: path, ext: "png"}
    |> custom("size", "#{ocr_source.width}x#{ocr_source.height}")
    |> canvas("white")
    |> Ocrlab.ImageUtil.draw_rectangles(text_areas, "black")
    |> Ocrlab.ImageUtil.draw_rectangles(non_text_areas, "white") 
    |> IO.inspect
    |> create()

    System.cmd("convert", 
      [path, ocr_source.file, 
        "-compose", "screen", "-composite", ocrable_path])
    ocrable_path
  end

  def extract_images(output_path, areas, ocr_source) do
    image = open(ocr_source.file)
    Enum.map(areas, fn area ->
      outfile = Path.join(output_path, "#{area.uuid}.jpg")
      custom(image, "crop", 
        "#{area.width}x#{area.height}+#{area.left}+#{area.top}")
      |> save(path: outfile)
      outfile
    end)
  end

  def scale_areas(areas, source, target) do
    height_ratio = target.height / source.height
    width_ratio = target.width / source.width
    
    Enum.map(areas, fn area ->
      %{area |
        width: (area.width * width_ratio),
        height: (area.height * height_ratio), 
        top: (area.top * height_ratio),
        left: (area.left * width_ratio),
      }
    end)
  end

  def scale_area(area, source, target) do
    height_ratio = target.height / source.height
    width_ratio = target.width / source.width

    %{area |
      width: (area.width * width_ratio),
      height: (area.height * height_ratio), 
      top: (area.top * height_ratio),
      left: (area.left * width_ratio),
    }
  end

  def initial_areas(pages) do
    filenames = Enum.map(pages, &(&1.gui_image.file))
    {result_json, code} = System.cmd("python", 
      ["scripts/document_analysis/main.py"] ++ filenames)

    {:ok, pages_results} = Jason.decode(result_json)

    Stream.zip(pages, pages_results)
    |> Enum.map(fn {page, [left, top, width, height]} ->
        big = box(left, top, width, height) 
        |> scale_area(page.gui_image, page.ocr_image)
    end)
  end

  def box(left, top, width, height) do
    %{left: left, top: top, width: width, height: height, type: "text"}
  end

end
