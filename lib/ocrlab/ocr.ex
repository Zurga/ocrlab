alias Ocrlab.Documents.Page

defmodule Ocrlab.Ocr do
  @word_breaks ~r/(\w)(-\n+)(\w)/u
  @line_breaks ~r/(\w)(\n+)([a-z])/u
  @bullet_mistakes ~r/^[¢e®»\*]\s/
  @line_marks ~r/[[:graph:]]/
  @multiple_spaces ~r/(^\s+|\f|\t|)/
  @empty_lines ~r/( \n)/
  @three_or_more_newlines ~r/\n{3,}/m
  @sentence_interruption ~r/([A-Z])([,\w ;\-]+)(\n+)(\w)/

  @hocr_line ~r/class='\w+_(line|carea|header|par|word)'/
  @bbox ~r/bbox (\d+) (\d+) (\d+) (\d+)/ 

  defmodule OcrResult do
    defstruct hocr: "", text: "", areas: []
  end

  def fix_line_breaks(text) do
    Regex.scan(@line_breaks, text)
    |> Enum.map(fn [word, _] -> 
      String.replace(word, "-\n", "")
    end)
  end

  def clean_text(text) do
    #text = Regex.replace(@line_breaks, text, "\\g{1} \\g{3}")
    #text = Regex.replace(@word_breaks, text, "\\g{1}\\g{3}")
    #text = Regex.replace(@sentence_interruption, text, "\\g{1}\\g{2} \\g{4}")
    text = Regex.replace(@bullet_mistakes, text, "\n- ")
    text
  end

  def ocr(ocrable_path) do
    params = ["--dpi", "300", "-l", "eng", ocrable_path, "-", "hocr"]
    {result, code} = System.cmd("tesseract", params)
    result
  end

  def extract_text(boxes) do
    Enum.reduce(boxes, "", fn box, text ->
      case box.type do
        "carea" -> text 
        "word" -> text <> Regex.replace(@bullet_mistakes, box.text <> " ", "- ")
        "par" -> text <> "\n\n"
        "line" -> text
        "header" -> text 
      end
    end)
    # |> clean_text()
  end

  # TODO change into cond for speed
  def line_type(line) do
    case Regex.run(@hocr_line, line) do
      nil -> nil
      # We have a result from the line
      [_, type] -> String.to_atom(type)
    end
  end

  def extract_boxes(html) do
    String.split(html, "\n")
    |> Enum.reduce([], fn line, acc -> 
      line = String.trim(line)

      case line_type(line) do
        nil -> acc
        # We have a result from the line
        type ->
          text = case type do
            "word" ->
              [_, text] = Regex.run(~r/>(.*)</, line)
              text
            _ -> ""
          end
          
          [x1, y1, x2, y2] = Regex.run(@bbox, line, capture: :all_but_first) 
                             |> Enum.map(&String.to_integer/1)
          acc ++ [%{
            left: x1,
            top: y1,
            width: x2 - x1,
            height: y2 - y1,
            type: type,
            text: text}]
      end
    end)
  end

  defp parse(line) do 
    [_, bbox, extra] = Regex.run(line, @bounding_box)
    [String.split(bbox, "\n"), extra]
  end
end
