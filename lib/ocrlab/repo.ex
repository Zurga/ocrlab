defmodule Ocrlab.Repo do
  use Ecto.Repo,
    otp_app: :ocrlab,
    adapter: Ecto.Adapters.Postgres
end
