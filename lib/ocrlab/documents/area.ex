defmodule Ocrlab.Documents.Area do
  use Ecto.Schema
  import Ecto.Changeset

  @required ~w(type left top width height uuid)a

  schema "areas" do
    field :type, :string
    field :order, :integer
    field :left, :float
    field :top, :float
    field :width, :float
    field :height, :float
    field :uuid, :string
    field :text, :string

    belongs_to :page, Ocrlab.Documents.Page
    timestamps()
  end

  @doc false
  def changeset(areas, attrs) do
    areas
    |> Map.update(:uuid, Ecto.UUID.generate, 
      fn val -> val || Ecto.UUID.generate end)
    |> cast(attrs, @required ++ [:page_id, :text])
    |> validate_required(@required)
  end
end
