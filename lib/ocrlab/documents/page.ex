defmodule Ocrlab.Documents.Page do
  use Ecto.Schema
  import Ecto.Changeset

  schema "pages" do
    field :number, :integer
    field :uuid, :string
    field :text, :string

    belongs_to :document, Ocrlab.Documents.Document

    has_many :text_areas, Ocrlab.Documents.Area, where: [type: "text"]
    has_many :image_areas, Ocrlab.Documents.Area, where: [type: "image"]
    has_many :word_areas, Ocrlab.Documents.Area, where: [type: "word"]
    has_many :line_areas, Ocrlab.Documents.Area, where: [type: "line"]
    has_many :par_areas, Ocrlab.Documents.Area, where: [type: "par"]
    has_many :carea_areas, Ocrlab.Documents.Area, where: [type: "carea"]

    has_one :gui_image, Ocrlab.Documents.Image, where: [type: "gui"]
    has_one :ocr_image, Ocrlab.Documents.Image, where: [type: "ocr"]

    timestamps()
  end

  @doc false
  def changeset(pages, attrs) do
    pages
    |> Map.update(:uuid, Ecto.UUID.generate, 
      fn val -> val || Ecto.UUID.generate end)
    |> cast(attrs, [:number, :uuid, :text, :document_id])
    |> validate_required([:number, :uuid])
  end
end
