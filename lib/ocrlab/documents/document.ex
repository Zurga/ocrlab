defmodule Ocrlab.Documents.Document do
  use Ecto.Schema
  use Arc.Ecto.Schema
  import Ecto.Changeset

  schema "documents" do
    field :name, :string
    field :file, Ocrlab.Document.Type
    field :uuid, :string
    field :text, :string, default: ""

    has_many :pages, Ocrlab.Documents.Page

    timestamps()
  end

  @doc false
  def changeset(document, attrs) do
    document
    |> Map.update(:uuid, Ecto.UUID.generate, 
      fn val -> val || Ecto.UUID.generate end)
    |> cast_attachments(attrs, [:file])
    |> cast(attrs, [:name, :text, :uuid])
    |> validate_required([:file])
  end
end
