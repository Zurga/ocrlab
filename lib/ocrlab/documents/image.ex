defmodule Ocrlab.Documents.Image do
  use Ecto.Schema
  import Ecto.Changeset

  schema "images" do
    field :file, :string
    field :height, :integer
    field :width, :integer
    field :resolution, :integer
    field :type, :string

    belongs_to :page, Ocrlab.Documents.Page

    timestamps()
  end

  @doc false
  def changeset(images, attrs) do
    images
    |> cast(attrs, [:file, :height, :width, :resolution, :type, :page_id])
    |> validate_required([:file, :height, :width, :resolution, :type])
  end
end
