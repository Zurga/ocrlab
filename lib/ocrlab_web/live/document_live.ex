defmodule OcrlabWeb.DocumentLive do 
  use Phoenix.LiveView, container: {:div, class: "liveview"}
  use Phoenix.HTML
  import Ocrlab.ImageUtil

  import Ocrlab.Ocr

  alias Ocrlab.Repo
  alias Poison.Encoder
  alias Poison.Decoder
  alias Ocrlab.Documents
  alias Ocrlab.Documents.{Page, Document, Area}

  import Ecto.Query

  #defstruct %UI{saved: false, compiled: false, ocr_finished: nil} end

  def render(assigns) do
    Phoenix.View.render(OcrlabWeb.EditView, "document_editor.html", assigns)
  end

  def mount(session, socket) do
    IO.puts "mounted"
    # if connected?(socket), do: :timer.send_interval(5000, self(), :autosave)
    document_id = session.current_document
    document = 
      case Cachex.get(:documents, document_id) do
        {:ok, nil} ->
          doc = Documents.get_document!(document_id)
          Cachex.put(:documents, document_id, doc)
          doc
        {:ok, document} -> 
          document
      end
    update_cache(document)
    areas = Enum.map(document.pages, fn page -> 
      {page.uuid, 
        scale_areas(page.text_areas, page.ocr_image, page.gui_image)
         ++ scale_areas(page.image_areas, page.ocr_image, page.gui_image)
      }
    end)
    {:ok, assign(socket, document: document, pages: document.pages, areas: areas,
      results: document.pages, focus: nil)}
  end

  def handle_event("save", _value, socket) do
    {:ok, document} = Cachex.get(:documents, socket.assigns.document.id)
    save(document)
    {:noreply, socket}
  end

  defp save(document) do
    # Documents.update_document(document, Map.from_struct(document))
    Documents.update_document(document, %{text: document.text})
    Enum.map(document.pages, fn page ->
      Documents.update_page(page, Map.from_struct(page))
      Enum.map(page.areas, fn area ->
        Documents.update_area(area, Map.from_struct(area))
      end)
    end)
  end

  def handle_info(:autosave, socket) do
    {:ok, document} = Cachex.get(:documents, socket.assigns.document.id)
    save(document)
    {:noreply, assign(socket, last_saved: now())}
  end
def handle_event("area-created", value, socket) do 
    {page_uuid, area, area_key} = page_id_and_area(value)
    {:ok, document} = Cachex.get(:documents, socket.assigns.document.id)
    page = find_by_uuid(document.pages, page_uuid)
    area_attrs = scale_area(area, page.gui_image, page.ocr_image)

    case Documents.create_area(page, area_attrs) do
      {:ok, area} -> 
        page = Map.put(page, area_key, Map.get(page, area_key) ++ [area])
        document = update_pages(document, page)
        update_cache(document)
        {:noreply, socket}
      {:error, reason} ->
        IO.inspect reason
        {:noreply, socket}
    end
  end

  def handle_event("area-removed", value, socket) do 
    {page_uuid, area, area_key} = page_id_and_area(value)

    {:ok, document} = Cachex.get(:documents, socket.assigns.document.id)
    page = find_by_uuid(document.pages, page_uuid)
    areas = Map.get(page, area_key)

    Documents.delete_area(find_by_uuid(areas, area.uuid))

    page = Map.put(page, area_key, remove_by_uuid(areas, area.uuid))
    document = update_pages(document, page)
    update_cache(document)
    {:noreply, socket}
  end

  def handle_event("area-updated", value, socket) do 
    IO.puts "area-updated"
    {page_uuid, area, area_key} = page_id_and_area(value)
    area_key = String.to_atom "#{area.type}_areas"
    {:ok, document} = Cachex.get(:documents, socket.assigns.document.id)
    page = find_by_uuid(document.pages, page_uuid)
    areas = Map.get(page, area_key)

    old_area = find_by_uuid(areas, area.uuid)
    area_attrs = scale_area(area, page.gui_image, page.ocr_image)

    case Documents.update_area(old_area, area_attrs) do
      {:ok, new_area} ->
        # TODO fix this
        page = Map.put(page, area_key, [new_area | remove_by_uuid(areas, area.uuid)])
        document = update_pages(document, page)
        update_cache(document)
        {:noreply, socket}
      {:error, changeset} ->
        IO.puts "Could not update image"
        IO.inspect changeset
        {:noreply, socket}
    end
  end

  def handle_event("initialize-areas", _value, socket) do
    {:ok, document} = Cachex.get(:documents, socket.assigns.document.id)

    # Delete existing areas
    Enum.map(document.pages, fn page ->
      from(a in Area, where: a.page_id == ^page.id and a.type == "text") 
      |> Repo.delete_all
    end)

    areas = initial_areas(document.pages)
    IO.inspect areas

    pages = Enum.zip(areas, document.pages)
      |> Enum.map(fn {area, page} ->
        %Page{page | text_areas: [area]}
      end)
    gui_areas = Enum.map(pages, fn page -> 
      {page.uuid, page.areas}
    end)
    document = %Document{document | pages: pages}
    update_cache(document)
    {:noreply, assign(socket, areas: gui_areas)}
  end

  def handle_event("ocr-page", %{"page_uuid" => page_uuid}, socket) do
    IO.puts "ocr-page"
    {:ok, document} = Cachex.get(:documents, socket.assigns.document.id)
    page = find_by_uuid(document.pages, page_uuid)
    page = Documents.ocr(document, page)

    document = update_pages(document, page)
    update_cache(document)
    {:noreply, socket}
  end

  def handle_event("render", %{"user_md" => user_md}, socket) do
    IO.puts "render"
    {:ok, document} = Cachex.get(:documents, socket.assigns.document.id)
    document = %Document{document | text: user_md}
    update_cache(document)
    {:noreply, assign(socket, document: document, result_shown: true, editor_shown: false)}
  end

  def handle_cast({:scroll_to, page}, socket) do 
    IO.puts "scroll_to"
    IO.inspect socket
    {:noreply, assign(socket, focus: page)}
  end

  defp page_id_and_area(query) do
    %{"page_uuid" => page_uuid, "area" => area} = URI.decode_query(query)
    area = for {key, val} <- Jason.decode!(area), into: %{} do 
      {String.to_atom(key), val}
    end
    {page_uuid, area, String.to_atom("#{area.type}_areas")}
  end

  defp remove_by_uuid(iterable, uuid) do
    Enum.reject(iterable, fn (other) -> 
      other.uuid == uuid end)
  end

  defp find_by_uuid(list, uuid) do
    Enum.find(list, fn(item) -> item.uuid == uuid end)
  end

  defp update_pages(document, page) do
    %Document{document |
      pages: Enum.sort(
        [page | Enum.reject(document.pages, fn (other) -> other.uuid == page.uuid end)], 
        &(&1.number < &2.number)
      )
    }
  end

  defp update_cache(document) do
    Cachex.put(:documents, document.id, document)
  end

  defp now() do
    Time.to_string(Time.add(Time.utc_now(), 2 * 3600, :second))
  end
end
