defmodule OcrlabWeb.EditorLive do
  use Phoenix.LiveView, container: {:div, class: "liveview"}
  use Phoenix.HTML

  import OcrlabWeb.EditView
  alias Ocrlab.Documents.Document
  alias Ocrlab.Documents

  def render(assigns) do
    Phoenix.View.render(OcrlabWeb.EditView, "editor.html", assigns)
  end

  def mount(session, socket) do
    {:ok, document} = Cachex.get(:documents, String.to_integer(socket.id))
    {:ok, assign(socket, document: document, results: document.pages)}
  end

  def handle_event("save-text", %{"user_md" => user_md}, socket) do
    IO.puts "saving"
    {:ok, document} = Cachex.get(:documents, socket.assigns.document.id)
    document = %Document{document | text: user_md}
    update_cache(document)
    {:noreply, socket}
  end

  def handle_event("focus", %{"page_uuid" => page_uuid}, socket) do
    IO.inspect socket
    GenServer.cast(socket.parent_pid, {:scroll_to, page_uuid})
    {:noreply, socket}
  end

  def handle_event("edit-word", _value, socket) do
    #TODO save edits done by the user in the OCR. 
    #TODO don't save edits having to do with hyphens
    {:noreply, socket}
  end

  def handle_event("clean-text", %{"document_uuid" => document_id}, socket) do
    IO.puts "clean text"
    {:ok, document} = Cachex.get(:documents, socket.assigns.document.id)
    # document = %Document{document | text: clean_text(document.text)}
    update_cache(document)
    {:noreply, assign(socket, document: document)}
  end

  defp update_cache(document) do
    Cachex.put(:documents, document.id, document)
  end

  def handle_event("ocr-all", _value, socket) do
    IO.puts "ocr-all"
    {:ok, document} = Cachex.get(:documents, socket.assigns.document.id)

    #document = %Document{document | 
    #  pages: Stream.reject(document.pages, 
    #    &(&1.text_areas == [] and &1.image_areas == []))
    #    |> Enum.map(&(Documents.ocr(document, &1)))
    #}
    document = Documents.ocr(document)
    # Put all the text in their respective pages.
    update_cache(document)
    IO.inspect Enum.reduce(document.pages, [], fn (page, acc) -> [page.text | acc] end)
    IO.puts "done"
    {:noreply, assign(socket, results: document.pages)}
  end

  def handle_info("ocr-page", %{"page_uuid" => page_uuid}, socket) do
    IO.puts "ocr-page"
    {:ok, document} = Cachex.get(:documents, socket.assigns.document.id)
    page = find_by_uuid(document.pages, page_uuid)
    page = Documents.ocr(document, page)

    document = update_pages(document, page)
    update_cache(document)
    {:noreply, socket}
  end

  defp find_by_uuid(list, uuid) do
    Enum.find(list, fn(item) -> item.uuid == uuid end)
  end

  defp update_pages(document, page) do
    %Document{document |
      pages: Enum.sort(
        [page | Enum.reject(document.pages, fn (other) -> other.uuid == page.uuid end)], 
        &(&1.number < &2.number)
      )
    }
  end

  defp update_cache(document) do
    Cachex.put(:documents, document.id, document)
  end
end
