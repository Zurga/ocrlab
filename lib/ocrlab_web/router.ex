defmodule OcrlabWeb.Router do
  use OcrlabWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", OcrlabWeb do
    pipe_through :browser

    get "/", PageController, :index
    post "/", PageController, :create

    resources "/edit", EditController, only: [:index, :edit]
  end

  # Other scopes may use custom stacks.
  # scope "/api", OcrlabWeb do
  #   pipe_through :api
  # end
end
