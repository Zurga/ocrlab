  def clean_ocr_result(html, _) when is_nil(html), do: ""
  def clean_ocr_result(pages) do
    pages
    |> Stream.reject(&(&1.text == nil))
    |> Stream.map(fn page ->
      page.text
      |> String.split("\n")
      |> Stream.reject(&(String.contains?(&1, "div")))
      |> Enum.reduce([], &(scrub(&1, &2, page))
    end)
    |> Stream.into([])
    |> Enum.join()
  end

  def scrub([line | result], acc, page) do 
    line
    |> Ocr.line_type()
    |> _scrub(rest, acc, page, false)
  end

  def _scrub(:word, [line | rest], result, page, skip) do
    case skip do 
      false -> 
        with text = get_text(line),
             line = insert_actions(line, page), 
             skip = String.ends_with?(text, "-")
        do
          _scrub(rest, [line | result], page, true)
        end
      true -> 
        with [previous | tail] = result,
             text = word(get_text(previous)) <> word(get_text(line)),
             line = Regex.replace(~r/>(.*)</, line, ">#{text}</")
                    |> insert_actions(page)
        do
          _scrub(rest, [line | tail], page, false)
        end
    end
  end

  def _scrub([line | rest], result, page, skip) do
    _scrub(Ocr.line_type(line), rest, result, page, skip)
  end
    
  def _scrub(rest, 
      type -> 
        if !skip do
          parse_line(rest, [line | result], page, skip)
        else
          parse_line(rest, result, page, skip)
        end
    end
  end
