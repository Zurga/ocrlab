defmodule OcrlabWeb.EditView do
  use OcrlabWeb, :view
  import Ocrlab.ImageUtil
  alias Ocrlab.Ocr
  alias Ocrlab.Spell

  def render_markdown(text) do 
    # TODO fix this so it only renders what has changed.
    # Store the rendered html in cache, check difference between lines 
    # and render only difference and insert them back into the cached html.
    case text do 
      nil ->
        ""
      "" -> 
        ""
      text -> 
        boxes = Ocr.extract_boxes(text)
        text = Ocr.extract_text(boxes)
        case Earmark.as_html(String.split(text, "\n")) do
        {_, html_doc, _} ->
          html_doc
        end
    end
  end

  def clean_ocr_result(html, _) when is_nil(html), do: ""
  def clean_ocr_result(pages) do
    pages
    |> Stream.reject(&(&1.text == nil))
    |> Stream.map(fn page ->
      page.text
      |> String.split("\n")
      |> Enum.reject(&(String.contains?(&1, "div")))
      |> parse_line(page)
    end)
    |> Stream.into([])
    |> Enum.join()
  end

  def parse_line(lines, page), do: parse_line(lines, [], page, false)
  def parse_line([], result, page, _), do: Enum.reverse(result)

  def parse_line([line | rest], result, page, skip) do
    case Ocr.line_type(line) do
      "word" -> 
        if skip do
          [previous | tail] = result
          text = word(get_text(previous)) <> word(get_text(line))
          line = Regex.replace(~r/>(.*)</, line, ">#{text}</")
                 |> insert_actions(page)
          parse_line(rest, [line | tail], page, false)
        else
          with text = get_text(line),
               line = insert_actions(line, page)
          do
            cond do
              # Hyphenated word
              String.ends_with?(text, "-") ->
                parse_line(rest, [line | result], page, true)
              true ->
                parse_line(rest, [line | result], page, false)
            end
          end
        end
      type -> 
        if !skip do
          parse_line(rest, [line | result], page, skip)
        else
          parse_line(rest, result, page, skip)
        end
    end
  end

  defp insert_actions(line, page) do
    word_conf = confidence(line)
    [tag, tag_rest] = String.split(line, ">", parts: 2)
    "#{tag} phx-focus=\"focus\" phx-value-page_uuid=\"#{page.uuid}\" phx-blur=\"edit-word\" style=\"#{word_conf}\" contenteditable=true>#{tag_rest}"
  end

  defp spelled(text) do 
    Spell.check(word(text))
  end

  defp get_text(line) do
    [_, text] = Regex.run(~r/>(.*)</, line)
    text
  end

  defp set_text(line, text) do 
    Regex.replace(~r/>(.*)</, line, text)
  end

  defp word(text) do
    if String.ends_with?(text, "-") do
      String.replace(text, "-", "")
    else 
      String.trim(text)
    end
  end

  def confidence(line) do 
    [_, confidence] = Regex.run(~r/x_wconf (\d+)/, line)
    badness = 0.8 - (String.to_integer(confidence) / 100)
    "background: rgb(0, 0, 255, #{badness})"
  end

end
