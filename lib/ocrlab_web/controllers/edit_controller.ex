defmodule OcrlabWeb.EditController do
  use OcrlabWeb, :controller

  def index(conn, %{"document" => document_id}) do
    render(conn, "index.html", document: 100)
  end

  def edit(conn, %{"id" => document_id}) do
    {document_id, _} = Integer.parse(document_id)
    render(conn, "index.html", document: document_id)
  end
end
