defmodule OcrlabWeb.PageController do
  use OcrlabWeb, :controller
  alias Ocrlab.Documents
  alias Ocrlab.Documents.Document
  import Routes

  def index(conn, _params) do
    changeset = Documents.change_document(%Document{})
    render(conn, "index.html", changeset: changeset)
  end

  def create(conn, %{"document" => document_params}) do
    case Documents.create_document(document_params) do
      {:ok, document} ->
        document
        redirect(conn, to: edit_path(conn, :edit, document.id))
      {:error, %Ecto.Changeset{} = changeset} ->
        IO.puts("something went wrong")
        IO.inspect(changeset)
        render(conn, "index.html", changeset: changeset)
    end
  end
end
